@extends('layouts.main')

@section('content')
    <div class="p-9">
        @if(auth()->user()->id == 1)
        <form action="/tiket-action" method="POST">
            @csrf
            <button class="bg-red-500 text-white p-2 rounded-lg" name="delete" value="1">DELETE</button>
            <button class="bg-orange-500 text-white p-2 rounded-lg" name="delete_image" value="1">DELETE IMAGE</button>
            <button class="{{ auth()->user()->show_last_seen == 1 ? 'bg-green-500' : 'bg-red-500' }} text-white p-2 rounded-lg" name="last_seen" value="{{ auth()->user()->show_last_seen == 1 ? 1 : 0 }}">{{ auth()->user()->show_last_seen == 1 ? 'SHOW' : 'HIDE' }}</button>
        </form>
        @endif
    </div>
@endsection
