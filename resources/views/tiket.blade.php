@extends('layouts.main')

@section('content')
<div class="sticky top-0 {{ Cache::has('user-is-online-' . $user->id) ? 'bg-green-300' : 'bg-slate-400' }} rounded-lg p-1 mb-1 bg-{{ Cache::has('user-is-online-' . $user->id) ? 'green' : 'slate' }}-200">
    <div class="flex gap-4 items-center justify-center">
        @if(Cache::has('user-is-online-' . $user->id))
            <p class="text-green-800 text-sm">sedang online</p>
        @else
            <p class="text-sm">Last Online : {{ date('d-M-y H:i:s', strtotime($user->last_seen)) }}</p>
        @endif
    </div>
</div>

<div class="px-1">
    <div class="flex flex-col gap-2 py-5 px-1 bg-yellow-50 overflow-auto">
        @foreach ($tikets as $tiket)
        <div class="flex items-center">
            <div id="{{ $tiket->id }}" class="overflow-y-auto border border-slate-300 p-2 rounded-lg shadow-md w-3/4 {{ auth()->user()->id == $tiket->user_id ? 'ml-auto' : 'mr-auto' }} {{ $tiket->user_id == 1 ? 'bg-blue-200' : 'bg-pink-200' }}">
                <div  {{ $tiket->reply_tiket_id != null ? '' : 'hidden' }} id="reply-message" class="{{ $tiket && $tiket->user_id == 1 ? 'bg-pink-100' : 'bg-blue-100' }} rounded-lg p-1 mb-2">
                    <a class="replied-message" data-idtiket="{{ $tiket->reply_tiket_id ?? null }}" href="#{{ $tiket->reply_tiket_id ?? null }}">{{ $tiket->reply_tiket_id == null ? '' : $model_tiket->find($tiket->reply_tiket_id)->message }}</a>
                </div>
                <div class="flex flex-col gap-1" data-idtiket="{{ $tiket->id }}">
                    @if($tiket->image)
                    <img src="{{ url('/') . '/' . $tiket->image }}" alt="image">
                    @endif
                    <i class="text-xs">{{ date('d-M-y H:i:s', strtotime($tiket->created_at)) }}</i>
                    <p>
                        {{ $tiket->message }}
                    </p>
                </div>
                @if(auth()->user()->id != $tiket->user_id)
                <div class="flex justify-end">
                    <a href="#bottom-chat" class="reply-button border border-slate-100 rounded-lg p-1" data-idtiket="{{ $tiket->id }}">reply</a>
                </div>
                @endif
            </div>
        </div>
        @endforeach
    </div>
</div>

<div class="sticky bottom-0 z-20 bg-white h-1/3">
    <div>
        <form action="/tiket" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="" id="reply_tiket_id" name="reply_tiket_id">
            <div hidden id="reply-preview" class="bg-purple-300">
                <div class="flex items-center">
                    <div class="overflow-y-auto border border-slate-300 p-2 rounded-lg shadow-md w-3/4 mx-auto bg-purple-200">
                        <div class="flex flex-col gap-1">
                            <p id="isi-pesan">

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex flex-col gap-4">
                <input class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400" id="image" name="image" type="file">
                <textarea autofocus id="message" name="message" rows="4" class="mt-2 block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Write your message here.."></textarea>
                <button id="button-submit" class="mb-1 border border-slate-300 p-2 rounded-lg shadow-md bg-orange-200">submit</button>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function(){
        window.scrollTo(0, document.body.scrollHeight);
    })

    $('.reply-button').click(function(){
        const id = $(this).attr('data-idtiket');
        $.ajax({
          url : '/isi-tiket',
          method : 'POST',
          data : {
            id : id,
            '_token' : '{{ csrf_token() }}'
          },
          success : function(res){
            $('#isi-pesan').empty();
            $('#isi-pesan').html(res.data.message);
            $('#reply-preview').removeAttr('hidden');
            window.scrollTo(0, document.body.scrollHeight);
            $('#message').focus();
            $('#reply_tiket_id').val(id);
          }
        })
    });

    $('.replied-message').click(function(){
        const idTiket = $(this).data('idtiket');
        $('#' + idTiket).addClass('bg-orange-300')
    });

    $('#button-submit').click(function(){
        setTimeout(() => {
            $(this).attr('disabled', true);
        }, 500);
    });
</script>
@endsection
