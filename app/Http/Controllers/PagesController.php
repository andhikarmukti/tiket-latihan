<?php

namespace App\Http\Controllers;

use App\Models\Tiket;
use App\Models\User;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function tiket()
    {
        $model_tiket = new Tiket();
        $tikets = $model_tiket->orderBy('created_at','desc')->take(100)->get()->reverse()->values();
        $user = User::all()->except(auth()->user()->id)->first();

        return view('tiket', compact(
            'tikets',
            'user',
            'model_tiket'
        ));
    }

    public function setting()
    {
        return view('setting');
    }
}
