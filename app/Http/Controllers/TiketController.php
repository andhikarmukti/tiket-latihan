<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Tiket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Http\Requests\StoreTiketRequest;
use App\Http\Requests\UpdateTiketRequest;
use Illuminate\Support\Facades\Validator;

class TiketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTiketRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTiketRequest $request)
    {
        $rules = [
            'image' => 'image|mimes:jpeg,png,jpg'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return back();
        }

        if(!$request->message && !$request->file('image')){
            return back();
        }
        if($request->file('image')){
            $image_path = $request->file('image')->store('images', ['disk' => 'public']);
        }

        Tiket::create([
            'user_id' => auth()->user()->id,
            'message' => !$request->message && $request->file('image') ? ' ' : $request->message,
            'reply_tiket_id' => $request->reply_tiket_id ?? null,
            'image' => $request->file('image') ? 'storage/' . $image_path : null
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function show(Tiket $tiket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function edit(Tiket $tiket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTiketRequest  $request
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTiketRequest $request, Tiket $tiket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tiket $tiket)
    {
        //
    }

    public function settingAction(Request $request)
    {
        // dd($request->delete);
        if($request->delete){
            $tikets = Tiket::all();
            foreach($tikets as $tiket){
                File::delete(public_path($tiket->image));
            }
            Tiket::truncate();
        }
        if($request->last_seen != null){
            User::where('id', 1)->update([
                'show_last_seen' => $request->last_seen == 0 ?? 1
            ]);
        }
        if($request->delete_image){
            $tikets = Tiket::all();
            foreach($tikets as $tiket){
                File::delete(public_path($tiket->image));
            }
        }
        return back();
    }

    // Ajax
    public function isiTiket(Request $request)
    {
        $tiket = Tiket::find($request->id);

        return response()->json([
            'success' => true,
            'data' => $tiket
        ], 200);
    }
}
