<?php

use App\Http\Controllers\PagesController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TiketController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('web_close');
})->name('webClose');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
// Route::middleware('webClose')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::get('/tiket', [PagesController::class, 'tiket']);
    Route::post('/tiket', [TiketController::class, 'store']);
    Route::post('/tiket-action', [TiketController::class, 'settingAction']);
    Route::get('/setting', [PagesController::class, 'setting']);

    // ajax
    Route::post('/isi-tiket', [TiketController::class, 'isiTiket']);
});

require __DIR__.'/auth.php';
